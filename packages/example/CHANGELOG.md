# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.6](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-example@0.1.5...@vtj/plugin-example@0.1.6) (2024-10-29)

**Note:** Version bump only for package @vtj/plugin-example





## [0.1.5](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-example@0.1.4...@vtj/plugin-example@0.1.5) (2024-10-28)

**Note:** Version bump only for package @vtj/plugin-example





## [0.1.4](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-example@0.1.3...@vtj/plugin-example@0.1.4) (2024-05-07)

**Note:** Version bump only for package @vtj/plugin-example





## [0.1.3](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-example@0.1.2...@vtj/plugin-example@0.1.3) (2024-05-07)

**Note:** Version bump only for package @vtj/plugin-example





## [0.1.2](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-example@0.1.1...@vtj/plugin-example@0.1.2) (2024-05-06)

**Note:** Version bump only for package @vtj/plugin-example





## 0.1.1 (2024-04-23)

**Note:** Version bump only for package @vtj/plugin-example

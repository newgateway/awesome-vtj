import { createPluginViteConfig, readJson } from '@vtj/cli';
const isUmd = !!process.env.UMD;
export default createPluginViteConfig({
  isUmd
});

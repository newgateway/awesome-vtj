# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.5](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.2.4...@vtj/plugin-ckeditor@0.2.5) (2024-10-30)


### Features

* ✨ ckeditor drak css ([a44e73f](https://gitee.com/newgateway/awesome-vtj/commits/a44e73ffb8540d893dc0dc66f75231f5bf23519d))





## [0.2.4](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.2.3...@vtj/plugin-ckeditor@0.2.4) (2024-10-30)


### Bug Fixes

* 🐛 ckeditor css ([d4307c3](https://gitee.com/newgateway/awesome-vtj/commits/d4307c3082d414a8c3f0ba45df1e472d9469ad14))





## [0.2.3](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.2.2...@vtj/plugin-ckeditor@0.2.3) (2024-10-29)


### Bug Fixes

* 🐛 ckeditor css ([690b5dc](https://gitee.com/newgateway/awesome-vtj/commits/690b5dc9fadd28356ea3f9edff7f5af375939b04))





## [0.2.2](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.2.1...@vtj/plugin-ckeditor@0.2.2) (2024-10-29)


### Bug Fixes

* 🐛 ckeditor css ([2fadb58](https://gitee.com/newgateway/awesome-vtj/commits/2fadb5826e81b1232c360209c3bee0adbded0457))





## [0.2.1](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.1.4...@vtj/plugin-ckeditor@0.2.1) (2024-10-28)

**Note:** Version bump only for package @vtj/plugin-ckeditor





## [0.1.4](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.1.3...@vtj/plugin-ckeditor@0.1.4) (2024-05-07)

**Note:** Version bump only for package @vtj/plugin-ckeditor





## [0.1.3](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.1.2...@vtj/plugin-ckeditor@0.1.3) (2024-05-07)

**Note:** Version bump only for package @vtj/plugin-ckeditor





## [0.1.2](https://gitee.com/newgateway/awesome-vtj/compare/@vtj/plugin-ckeditor@0.1.1...@vtj/plugin-ckeditor@0.1.2) (2024-05-06)

**Note:** Version bump only for package @vtj/plugin-ckeditor





## 0.1.1 (2024-04-24)


### Features

* ✨ add ckeditor ([f06a071](https://gitee.com/newgateway/awesome-vtj/commits/f06a0717762192a3ee32c52cd78a7f4bd67275f0))

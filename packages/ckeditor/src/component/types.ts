import { type PropType } from 'vue';
import type { ComponentPropsType, UploaderResponse } from '@vtj/ui';

export type CKEditorImageUploder = (
  file: File
) => Promise<string | UploaderResponse | null>;

export const ckeditorProps = {
  /**
   * 内容
   */
  modelValue: {
    type: String,
    default: ''
  },
  /**
   * 禁用
   */
  disabled: {
    type: Boolean
  },
  /**
   * 可编辑状态
   */
  editable: {
    type: Boolean,
    default: true
  },
  /**
   * 显示边框, 当 editable=false 时有效
   */
  border: {
    type: Boolean,
    default: true
  },
  /**
   * 图片上传函数
   */
  uploader: {
    type: Function as PropType<CKEditorImageUploder>
  },
  /**
   * 编辑器内容高度
   */
  height: {
    type: String,
    default: 'auto'
  },

  fit: {
    type: Boolean
  }
};

export type CKEditorProps = ComponentPropsType<typeof ckeditorProps>;

export type CKEditorEmits = {
  'update:modelValue': [value?: string];
  change: [value?: string];
  blur: [value: string, e: any];
  focus: [e: any];
  ready: [editor: any];
};

import { createViteConfig, createPluginViteConfig } from '@vtj/cli';
import { createDevTools } from '@vtj/pro/vite';
const isUmd = !!process.env.UMD;
const isDev = !!process.env.DEV;

export default isDev
  ? createViteConfig({
      plugins: [createDevTools()]
    })
  : createPluginViteConfig({
      isUmd
    });

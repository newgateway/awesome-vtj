import { resolve } from 'path';
import { createViteConfig } from '@vtj/cli';

export default createViteConfig({
  host: '0.0.0.0',
  https: false,
  legacy: false,
  elementPlus: true,
  babel: false,
  plugins: [],
  alias: {
    '@vtj/plugin-example': resolve('../packages/example/src'),
    '@vtj/plugin-ckeditor': resolve('../packages/ckeditor/src')
  }
});

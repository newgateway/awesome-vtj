import { createRouter, createWebHashHistory } from 'vue-router';
import { pageRoutes } from '../modules';

const routes: any = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/index.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@/views/not-found.vue')
  },
  ...pageRoutes.value
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;

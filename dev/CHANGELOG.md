# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.7.44](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.43...dev-web@0.7.44) (2024-10-30)

**Note:** Version bump only for package dev-web





## [0.7.43](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.42...dev-web@0.7.43) (2024-10-30)

**Note:** Version bump only for package dev-web





## [0.7.42](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.41...dev-web@0.7.42) (2024-10-29)


### Bug Fixes

* 🐛 ckeditor css ([690b5dc](https://gitee.com/newgateway/awesome-vtj/commits/690b5dc9fadd28356ea3f9edff7f5af375939b04))





## [0.7.41](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.40...dev-web@0.7.41) (2024-10-29)

**Note:** Version bump only for package dev-web





## [0.7.40](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.39...dev-web@0.7.40) (2024-10-28)

**Note:** Version bump only for package dev-web





## [0.7.39](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.38...dev-web@0.7.39) (2024-05-07)

**Note:** Version bump only for package dev-web





## [0.7.38](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.37...dev-web@0.7.38) (2024-05-07)

**Note:** Version bump only for package dev-web





## [0.7.37](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.36...dev-web@0.7.37) (2024-05-06)

**Note:** Version bump only for package dev-web





## [0.7.36](https://gitee.com/newgateway/awesome-vtj/compare/dev-web@0.7.35...dev-web@0.7.36) (2024-04-24)


### Features

* ✨ add ckeditor ([f06a071](https://gitee.com/newgateway/awesome-vtj/commits/f06a0717762192a3ee32c52cd78a7f4bd67275f0))





## 0.7.35 (2024-04-23)

**Note:** Version bump only for package dev-web






## [0.7.34](https://gitee.com/newgateway/vtj/compare/dev-web@0.7.33...dev-web@0.7.34) (2024-04-10)

**Note:** Version bump only for package dev-web






## [0.7.33](https://gitee.com/newgateway/vtj/compare/dev-web@0.7.32...dev-web@0.7.33) (2024-04-08)


### Features

* ✨ ui add XCkeditor ([2870b53](https://gitee.com/newgateway/vtj/commits/2870b538869530b0f703ebd85d462bf7769ed262))






## [0.7.32](https://gitee.com/newgateway/vtj/compare/dev-web@0.7.31...dev-web@0.7.32) (2024-04-03)

**Note:** Version bump only for package dev-web






## [0.7.31](https://gitee.com/newgateway/vtj/compare/dev-web@0.7.30...dev-web@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no






## [0.7.31](https://gitee.com/newgateway/vtj/compare/dev-web@0.7.30...dev-web@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no
